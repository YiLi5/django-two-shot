from django.contrib import admin
from receipts.models import ExpenseCategory, Account, Receipt


@admin.register(ExpenseCategory)
class ExpenseCategoryAdmin(admin.ModelAdmin):
    ()


@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    ()


@admin.register(Receipt)
class ReceiptAdmin(admin.ModelAdmin):
    ()
